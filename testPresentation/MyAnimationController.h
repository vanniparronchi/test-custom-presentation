#import <UIKit/UIKit.h>

@interface MyAnimationController : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic, assign, getter=isPresenting) BOOL presenting;
@end
