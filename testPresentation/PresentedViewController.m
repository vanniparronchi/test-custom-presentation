#import "PresentedViewController.h"

@implementation PresentedViewController
- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
