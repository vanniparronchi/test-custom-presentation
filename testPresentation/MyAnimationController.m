#import "MyAnimationController.h"

@implementation MyAnimationController

- (instancetype)init
{
    self = [super init];
    if (self) {
        _presenting = YES;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.8;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIView *containerView = [transitionContext containerView];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    
    
    if (self.presenting) {
        toView.transform = CGAffineTransformMakeScale(1.4, 1.4);
        toView.frame = containerView.bounds;
        [containerView addSubview:toView];
    }
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.4 initialSpringVelocity:0.0 options:0 animations:^{
        if (self.presenting) {
            toView.transform = CGAffineTransformIdentity;
        } else {
            fromView.transform = CGAffineTransformMakeScale(3.0, 3.0);
            fromView.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
}

@end
