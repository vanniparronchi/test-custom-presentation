//
//  main.m
//  testPresentation
//
//  Created by Vanni Parronchi on 05/09/15.
//  Copyright (c) 2015 Thumbspire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
