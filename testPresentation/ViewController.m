#import "ViewController.h"
#import "MyTransitioningDelegate.h"

@interface ViewController ()
@property (nonatomic, strong) MyTransitioningDelegate *td;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.td = [[MyTransitioningDelegate alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonTapped:(id)sender
{
    UIViewController *presentedVC = [self.storyboard instantiateViewControllerWithIdentifier:@"presented_vc"];
    presentedVC.modalPresentationStyle = UIModalPresentationCustom;
    presentedVC.transitioningDelegate = self.td;
    
    [self presentViewController:presentedVC animated:YES completion:nil];
}

@end
