#import "MyTransitioningDelegate.h"
#import "MyPresentationController.h"
#import "MyAnimationController.h"

@implementation MyTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented
                                                      presentingViewController:(UIViewController *)presenting
                                                          sourceViewController:(UIViewController *)source
{
    return [[MyPresentationController alloc] initWithPresentedViewController:presented
                                                    presentingViewController:presenting];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    MyAnimationController *animCtr =  [[MyAnimationController alloc] init];
    animCtr.presenting = YES;
    return animCtr;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    MyAnimationController *animCtr =  [[MyAnimationController alloc] init];
    animCtr.presenting = NO;
    return animCtr;
}

@end
