
#import "MyPresentationController.h"

@interface MyPresentationController ()
@property (nonatomic, strong) UIView *dimmingView;
@end

@implementation MyPresentationController

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController
{
    if (self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController]) {
        _dimmingView = [[UIView alloc] init];
        _dimmingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
        _dimmingView.alpha = 0.0;
    }
    
    return self;
}

- (void)presentationTransitionWillBegin
{
    [self.containerView addSubview:self.dimmingView];
    [self.dimmingView addSubview:self.presentedViewController.view];
    self.dimmingView.frame = self.presentingViewController.view.bounds;

    
    id<UIViewControllerTransitionCoordinator> tc = [self.presentingViewController transitionCoordinator];
    
    self.dimmingView.alpha = 0.0;
    [tc animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmingView.alpha = 1.0;
    } completion:nil];
}

- (void)dismissalTransitionWillBegin
{
    id<UIViewControllerTransitionCoordinator> tc = [self.presentingViewController transitionCoordinator];
    
    self.dimmingView.alpha = 1.0;
    [tc animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmingView.alpha = 0.0;
    } completion:nil];
}

- (void)presentationTransitionDidEnd:(BOOL)completed
{
    if (!completed) {
        [self.dimmingView removeFromSuperview];
    }
}

- (CGRect)frameOfPresentedViewInContainerView
{
    CGRect bounds = self.containerView.bounds;
    return CGRectInset(bounds, 40.0, 60.0);
}


@end
